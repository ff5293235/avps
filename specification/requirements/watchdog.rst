.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Watchdog
--------

**Discussion:**

A watchdog is a device that supervises that a system is running by using
a counter that periodically needs to be reset by software. If the
software fails to reset the counter, the watchdog assumes that the
system is not working anymore and takes measures to restore system
functionality, e.g., by rebooting the system. Watchdogs are a crucial
part of safety-concerned systems as they detect misbehavior and stop a
possibly harming system.

In a virtualized environment, the hypervisor shall be able to supervise
that the guest works as expected. By providing a VM with a virtual watchdog
device, the hypervisor can observe whether the guest regularly updates
its watchdog device, and if the guest fails to update its watchdog, the
hypervisor can take appropriate measures to ensure a possible
misbehavior and to restore proper service, e.g., by restarting the VM.

While a hypervisor might have non-cooperating means to supervise a
guest, being in full control over it, using a watchdog is a
straight-forward and easy way to implement a supervision functionality.
An implementation is split in two parts, one being the in the
hypervisor, the device, and another in the guest operating system, a
driver for the device offered by the hypervisor. As modifying and adding
additional drivers to an operating system might be troublesome because
of the effort required, it is desirable to use a watchdog driver that is
already available in guest operating systems.

There are no standardized watchdog but one was defined for Arm platforms
in the Base System Architecture [BSA] Appendix C. We therefore recommend hypervisors 
to implement the watchdog according to the generic watchdog described in [BSA],
not only on Arm systems but on all architectures.

The default action is assumed to be a reboot of the AVP machine but it 
shall be possible for the platform owner to define a policy through the
OAM interface. Such policies can be "trigger rebooting a set of AVP machines", 
or "shutingdown the whole system".

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-WDG-1}
    - An Arm platform MUST implement a virtual hardware
      interface following the generic watchdog
      described in Base System Architecture [BSA]
  * - {AVPS-WDG-2}
    - Non Arm platform SHOULD implement a virtual hardware
      interface following the generic watchdog
      described in Base System Architecture [BSA]

.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
GPIO
----

**Discussion**:

GPIOs are typically simple devices that consist of a set of pins that
can be operated in input or output mode. Each pin can be either on or
off, sensing a state in input mode, or driving a state in output mode.
For example, GPIOs can be used for sensing buttons and switching, or
driving LEDs or even communication protocols. Hardware-wise a set of
pins forms a GPIO block that is handled by a GPIO controller.

In a virtualization setup, a guest might want to control a whole GPIO
block or just single pins. For a GPIO block that is provided by a GPIO
controller, the hypervisor can pass-through the controller so that the
guest can directly use the device with its appropriate drivers. If
pins on a single GPIO block shall be shared across multiple guests, or
a guest shall not have access to all pins of a block, the hypervisor
must multiplex access to this block. A GPIO device exists in VIRTIO
[VIRTIO-GPIO] along with backends [VHOST-DEVICE] that allow for
multiplexing individual pins from a host GPIO to multiple guests.

Usage of GPIOs for time-sensitive use, such as “bit-banging”, is not
recommended because it requires a particular scheduling of the guest.
For such cases, the virtual platform should provide other suitable means
to implement a driver for the functionality that is being emulated by
bit-banging.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GPIO-1}
    - The hypervisor/equivalent shall support configurable
      pass-through access to a VM for digital general-purpose I/O hardware
  * - {AVPS-GPIO-2}
    - The platform MAY provide multiplexed access to host GPIO pins to the
      virtual platform via [VIRTIO] (ref: [VIRTIO-GPIO])

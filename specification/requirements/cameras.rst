.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Cameras
-------

**Discussion:**

Potential in-car architectures:

- Separate Camera-ECU communicating to head unit via some network
- Head unit is connected directly to the camera sensor
- Cameras used entirely for object recognition (no need to connect
  to an ECU that drives a user-facing display)

Also integrated image processors and other camera-dedicated silicon is
likely to be increasing (also built into generic SoCs), to simplify
advanced calculation such as intelligent object/obstacle recognition,
and stitching multiple cameras into “bird’s eye view”, etc.

The existing [VIRTIO-video] specification draft defines only encoder and
decoder capabilities. The alternative [VIRTIO-media] proposal exposes
the Linux V4L2 API to guests which works well for webcam type
applications. The camera problem space is broad and the community may
not come up with a single solution which covers the broad range
between simple webcam devices to the complex computationally enhanced
sensors that appear on mobile devices.

Different pieces of camera hardware may have different capabilities and
limitations, such as how many settings can be set individually for each
sensor. Some settings are thus forced to be identical for all, or for
groups of cameras. Some of the complexities include, but are not limited
to:

Camera sensors might have different characteristics: the serial
interface bandwidth can differ for different connected sensors to take
potential higher and lower resolution capture requirements into account,
but in other cases, hardware control (resolution, frame rate, ...) are
set in groups, so you can only set the same setting for all, or a group
of cameras.

Since all the camera sensors are usually routed to a single DMA engine,
it is then up to the paravirtualized solution to provide buffer sharing
mechanism to receive buffers from the driver and fill those with data
the same way it is done for the video codec sharing.

Image processing / stitching is often built into the camera device (or
ECU), so it is unlikely to be modified by a virtual hardware /
hypervisor layer.

Some stateless cameras allow reconfiguration all the way down to
individual frames, which would theoretically allow different VMs to have
different configurations while sharing the same camera. Whenever
possible, the hypervisor should utilize this capability to provide
seamless access to the camera sensors and their settings in the most
flexible way that is possible. When the camera device does not support
the stateless operation mode, the hypervisor could emulate this mode but
restrict access to the camera to only one or more clients at a time
according to the limitations that the real hardware has regarding the
individual sensor configuration.

For future reference and study, Xen has Webcam use on a
laptop/workstation is the likely driving use case, as opposed to the
multiple cameras of an automotive system. It is defined using a Xen
specific protocol which is not specified by VIRTIO and has been
accepted by the Xen and Linux kernel community. At the time of
writing, the front-end driver is created but not yet merged in
mainline Linux.

Above, we argued against advanced emulation of camera capabilities in
the hypervisor layer, and thus the hypervisor is expected to put limits
on, or negotiate, and ultimately allow only the configurability that the
hardware allows for.

**AVPS Requirements**:

.. todo:: Since we are not yet aware that a proposal similar to “VIRTIO-camera”
  has started yet, no requirements are defined at this time.
  Potential for future work exists here.

.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Audio
-----

**Discussion**:

There has been support for audio in the VIRTIO specification since the
1.2 release (see :ref:`VIRTIO-SND<references>`). It includes how the
Hypervisor can report audio capabilities to the guest, such as
input/output (microphone/speaker) capabilities and what data formats
are supported when sending audio streams.

Sampled data is expected to be in PCM format, but the details are
defined such as resolution (number of bits), sampling rate (frame rate)
and the number of available audio channels and so on.

Most such capabilities are defined independently for each stream. One VM
can open multiple audio streams towards the Hypervisor. A stream can
include more than one channel (interleaved data, according to previous
agreement of format).

The virtual audio card definition also supports controls. The controls can be
used to set the volume level, mute/unmute the audio signal, switch different
modes/states of the virtual sound device in Hypervisor. The control support is
guarded by a separate virtio feature bit.

Challenges include the real-time behavior, keeping low latency in the
transfer, avoiding buffer underruns, etc. Determined reliability may
also be required by some safety-critical audio functions and the
separation of audio with varying criticality is required, although
sometimes this is handled by preloading
chimes/sounds into some media hardware and triggered through another
event interface.

State transitions can be fed into the stream. Start, Stop, Pause,
Unpause. These transitions can trigger actions. For example, when
navigation starts playing, you can lower the volume of media.

Start means start playing the samples from the buffer (which was earlier
filled with data) (and opposite for input case). Pause means stop at the
current location, do not reset internal state, so that unpause can
continue playing at that location.

It is also possible to be informed about buffer overruns from the virtual sound
device in the guest. The functionality is guarded by a separate virtio feature
bit.

A Linux driver for VirtIO sound has been available since v5.13. As of early 2024 audio controls
support is targeting latest kernel releases as well (it has been merged to "sound-next"
branch of the Linux kernel in February 2024). A
back end implementation has existed in QEMU since release 8.2. You can also
off-load the device emulation to a hypervisor agnostic separate
process (see :ref:`VHOST-DEVICE<references>`) if required.
Previously QEMU played audio by hardware emulation of a sound card,
whereas this new approach is using VIRTIO.

**Potential future requirements:**

.. todo:: Assign audio requirement
    
.. list-table:: 
  :widths: 20 80
  
  * - [PENDING]
    - If virtualized audio is implemented it MUST implement the VIRTIO-sound standard according to [VIRTIO-SND].

There are several settings / feature flags that should be evaluated to
see which ones shall be mandatory required on an automotive platform.


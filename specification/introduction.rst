.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../macros.rst
============
Introduction
============

This specification covers a collection of virtual interfaces and other requirements for running virtualized workloads. The interfaces are defined between virtual machines and the virtualization infrastructure, i.e. the hypervisor or virtualization "host system". Even though the internal behavior of the virtualization infrastructure is mostly irrelevant to the virtualized workloads execution, some aspects such as workload startup configuration or device firmware update, need to be exposed.

Despite is broad applicability, the Automotive Virtual Platform Specification (AVPS), defines the interfaces and requirements between AVP Machines and AVP Infrastructure so that an automotive workload can be executed without changes,following relevant safety and cybersecurity standards, in an AVP Machine created by any AVP Infrastructure implementation.

A brief history
---------------

Initially, a working group within the Hypervisor Project, within the GENIVI Alliance, prepared this specification, and a good deal of information was provided by sources outside the automotive industry.
GENIVI, now COVESA, develops standard approaches for integrating operating systems and middleware in automotive systems and promotes a common vehicle data model and standard service catalog for use in-vehicle and in the vehicle cloud.
With GENIVI refocusing, and renaming itself to COVESA, the hypervisor working group was dissolved and the AVPS release 2.0 was the last release by the initial hypervisor working group.
Some time later, hypervisor and virtualization enthusiasts formed a Hypervisor Working Group under the umbrella of the  Scalable Open Architecture for Embedded Edge (SOAFEE) project. SOAFEE is an industry-led collaboration defined by automakers, semiconductor suppliers, open source and independent software vendors, and cloud technology leaders.

After a formal hand-over from COVESA, the AVPS found a new home in the Hypervisor Working Group of SOAFEE, contributing to the goal of defining interoperability across virtualization solutions.
The group's focus is to work with SOAFEE members and others on elaborating this specification and a hypervisor portability guide for Arm. Another SOAFEE group is leveraging those specifications and guide to build an open source reference stack: Edge Workload Abstraction and Orchestration Layer (EWAOL).

The specification remains open licensed and contributions to the specification are very welcome.

Motivation
----------

Automotive systems use software stacks with particular needs. Existing standards for virtualization sometimes need to be augmented, partly because their original design was not based on automotive or embedded systems. The industry needs a common initiative to define the basics of virtualization as it pertains to Automotive whereas much of the progress in virtualization has come from IT/server consolidation and a smaller part from the virtualization of workstation/desktop systems. Embedded systems are still at an early stage, but the use of virtualization is increasing, and is starting to appear also in the upstream project initiatives that this specification relies heavily upon, such as VIRTIO.

A shared virtual platform definition in automotive creates many advantages:

- It simplifies moving hypervisor guests between different hypervisor environments.
- It can over time simplify reuse of existing legacy systems in new, virtualized, setups.
- Device drivers for paravirtualization, for operating system kernels (e.g. Linux) do not need to be maintained uniquely for different hypervisors.
- There is some potential for shared implementation across guest operating systems.
- There is some potential for shared implementation across hypervisors with different license models.

A specification can enable industry shared requirements and test suites, a common vocabulary and understanding to reduce the complexity of virtualization.

As a comparison, the OCI Initiative for Linux containers successfully served a similar purpose. There are now several compatible container runtimes, and synergy effects added to the most obvious effects of standardization. Similarly, there is potential for standardized hypervisor runtime environments that promote portability and allow a standards-compliant virtual (guest) machine to run with significantly less integration efforts.

Hypervisors can fulfill this specification and claim to be compliant with its standard, still leaving opportunity for local optimizations and competitive advantages. Guest virtual machine (VMs) can be engineered to match the specification. In combination, this leads to a better shared industry understanding of how virtualization features are expected to behave, reduced software integration efforts, efficient portability of legacy systems and futureproofing of designs, as well as lower risks when starting product development.

Specification outline
---------------------

The AVPS specification is intended to be immediately usable as a set of platform requirements, 
but also to start the conversation about further standardization and provide guidance for discussions 
between implementers and users of compatible virtualized systems that follow this specification. 
Each area is therefore split in a discussion section and a requirement section. The requirement section is the normative part. 
(See the section :numref:`Conformance` :ref:`Conformance` for further guidance on adherence to the specification).

Each discussion section outlines various non-normative considerations in addition to the firm requirements. It also provides rationale for the requirements that have been written (or occasionally for why requirements were not written), and it often serves to summarize the state-of-the-art situation in each area.

Processor architecture and hardware considerations
--------------------------------------------------

This specification is intended to be processor architecture agnostic (i.e. applicable for Intel |(r)|, Arm |(r)| [#]_ among others) and hardware selection independent. We welcome all input to further progress towards that goal.

.. note:: We should recognize, however, that all the software mentioned here (i.e. the hypervisor, and the virtual machine guests that execute kernels and applications) is machine code that is compiled for the real hardware’s specific CPU architecture. For anyone who is new to this technology it is worthwhile to point out that it is not an emulation/interpreter layer for individual instructions such as that used to execute CPU-independent “byte-code” in a Java virtual machine. While this understanding is often assumed in similar documents, we point this out because such byte-code interpreters are inconveniently also called “virtual machines”. This specification deals with hardware virtualization and hypervisors – i.e. execution environments whose virtual machine environments mimic the native hardware and can execute the same programs. The consequence is therefore that some differences in hardware CPU architectures and System- on-chip (SoC) capabilities must be carefully studied.

Some referenced external specifications are written by a specific processor technology provider, such as Arm, and define interface standards for the software/hardware interface in systems based on that hardware architecture. We found that some such interface specifications could be more widely applicable – that is, they are not too strongly hardware architecture dependent. The AVPS may therefore reference parts of those specifications to define the interface to virtual hardware. The intention is that the chosen parts of those specifications, despite being published by a hardware technology provider, should be possible to implement on a virtual platform that executes on any hardware architecture.

.. todo:: The advences of Confidential Computing will introduce new elements related to trustworthiness, development and deployment life cycles. Continuous work should be done to unify the interfaces in this virtual platform definition to achieve improved hardware portability.

Heterogeneous compute considerations
------------------------------------

AVP may coexist with additional compute resources such as:
- Secure Element 
- Cortex-R hosted AUTOSAR application. 
- Trusted Execution Environments (isolated AVP processor execution mode)

Writing software for those environments are outside the scope of the present specification. Yet, provisionning and/or communication with those compute resources is needed. As a result, AVPS interfaces scope includes standard communication facillities between AVP and those heterogeneous compute domains.

Hardware pass-through
---------------------

In a non-virtualization environment, a single operating system kernel accesses the hardware. Whereas, a virtual platform, based on a hypervisor, typically acts as an abstraction layer over the actual hardware. This layer enables multiple virtual machines (VMs), each running their own operating system kernel, to access a single set of hardware resources. The process of enabling simultaneous access to hardware from (multiple) VMs is called virtualization.

The hypervisor abstraction layer exposes virtual hardware, which has an implementation below its access interface, to sequence, parallelize, or arbitrate between requests to the real hardware resource.
Virtual hardware can be abstract (such as virtio) or emulated (software implementation of real hardware). It should be a goal of virtualization communities to make every efforts possible to design and code an abstract hardware instead of coding an emulation hardware.

Exposing virtual hardware can present a performance and/or resource cost. So there may be a desire to give VMs access to the real hardware through a process called “hardware pass-through”.

While tempting, this approach raise both technology and security challenges that impose a thourough analysis to be employed in automotive.

On the technology side, unless the hardware has built-in features for parallel usage, pass-through effectively reserves the hardware for a specific VM and makes it unavailable to other VMs. This assumes that access privileges to MMIO control zones can be effectively restricted to that VM (hypervisor compatible page size alignment required). Furthermore, in that "assigned" use case, a single device may depend on clock and power controls that are shared amonst devices and thus defeats the goal of assigning the device. 

On the security side, accessing hardware directly typically jeopardizes isolation of multi-tenant virtualization infrastructure. For instance, isolating "ADAS" VMs from the powertrain VMs that are deployed accross network connected vechicle zones . Giving direct access to the network/disks may result in one tenant being able to communicate/access data without control to other tenants, possibly spreading "infections". 

.. note:: while technically feasible, virtualization infrastructure extension to a SmartNIC that has PCI Express SR-IOV sharing capacity would introduce costs and complexity that may not be acceptable in the SDV space.

The net result is that using hardware pass-through in SDV environments is not recommended.


Virtualization implementation designs
-------------------------------------

Comparing virtualization solutions can be difficult due to differing internal designs. Sometimes these are real and effective differences whereas sometimes only different names are used for mostly equivalent solutions, or simply different emphasis is placed on certain aspects.

When developping in the cloud, virtualization can be provided by emulators such as Qemu or Arm Virtual Hardware, which may leverage a hypervisor to accelerate part of its operations. Other platforms match a simple model often assumed in descriptions in this document, in which a single component named “hypervisor” is, in effect, the implementation of the entire virtual platform.

Some others conversely state that their actual hypervisor is minimal, and highlight especially the fact that the purpose of the hypervisor is only to provide separation and scheduling of individual virtual machines – thus it implements a kind of “separation kernel”. The remaining features of those designs might then be delegated to dedicated VMs, either with unique privileges or even VMs that are considered almost identical in nature to the “guest” VMs. Consequently, it is then those provided VMs that implement some of the API of the virtual platform, i.e. the APIs available for use by the “guest” VMs.

Some environments use different nomenclature and highlight the fact that parts of the virtual platform may have multiple privilege levels, “domains”. These are additional levels defined beyond the simple model of: user space < OS kernel < Hypervisor.

Some offerings propose that real-time functions can be run using any Real-Time Operating System (RTOS) that runs as a guest operating system in one VM, as an equivalent peer to a general-purpose VM (running Linux kernel for example). Whereas others put emphasis on their implementation being an RTOS kernel first, that provides direct support for real-time processes/tasks (like an OS kernel), and where the RTOS kernel simultaneously acts as a hypervisor towards foreign/guest kernels. The simple description of this without more deeply defining the actual technical design would be that it is an RTOS kernel that also implements a hypervisor.

The design of the hypervisor/virtualization platform and the design of the full system (including guest VMs) sometimes tend to be interdependent, but the intention of this specification is to try to be independent of such design choices. Although other goals for this specification have also been explained, starting with portability of guest VMs should bring any concerns to the surface. If an implementation can follow the specification and ensure such portability, then the actual technical design of the Hypervisor or virtual platform is free to vary.

These differences of philosophy and design are akin to the discussion of monolithic kernels vs. microkernels in the realm of operating systems but here the discussion is about the “virtual platform kernel” (i.e. hypervisor) instead.

Suffice to say that this specification does not strive to advocate only one approach or limit the design of the virtualization platforms, but still strives to maximize compatibility and shared advancement and therefore focus primarily on defining the APIs between a guest VM (operating system kernel) and the virtual platform it runs on.

Please be aware that certain parts of the discussion sections may still speak about the “Hypervisor” implementing a feature, but it should then be interpreted loosely, i.e. it should fit also designs that provide identical feature compatibility but has the implementation delegated to some type of VM.

Draft specification state
-------------------------

Some areas of the platform are not ready for a firm definition of a standard virtual platform. The reasons could include:

- There are sometimes proposals made upstream in VIRTIO, for example, that appear to fit the needs of a virtual platform but are not yet approved and are still under heavy discussion. The AVPS working group then considered it better to wait for these areas to stabilize.
- The subject is complex and requires further study. Implementations of virtual hardware devices in this area might not even exist for this reason and we therefore defer to a pass-through solution. As an intermediary, the feature may be implemented with a simple hardware driver that exposes a bespoke interface to the VMs but does not make it available to multiple VMs as virtual-hardware and is neither subject for standardization.
- It could be an area of significant innovation and hypervisor implementers therefore wish to be unencumbered by requirements at this time, to either allow for free investigation into new methods, or for differentiating their product.
- The subject area might not have any of the above issues, but the working group has not had time and resources to cover it yet. *This is an open and collaborative process, so input is welcome from additional contributors*.

For situations described above, there are no requirements yet, but we have sometimes kept the discussion section
to introduce the situation, and to prepare for further work. Whereas all parts are open for improvement in later versions, 
some chapters will have the explicit marker

.. todo:: Potential for future work exists here.

We invite volunteers to provide input or write those chapters. Joining the working group discussions would be a welcome first step.

.. rubric:: Footnotes

.. [#] Arm is a registered trademark of Arm Limited (or its subsidiaries) in the US and/or elsewhere

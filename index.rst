.. SPDX-License-Identifier: CC-BY-SA-4.0

Automotive Virtual Platform Specification
=========================================

.. toctree::
   :numbered:
   :maxdepth: 2
   :includehidden:
   :caption: Contents
   
   README.rst
   specification/introduction.rst
   specification/avp.rst
   specification/requirements.rst
   specification/references.rst
   TODO.rst
   CONTRIBUTING.rst

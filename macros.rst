.. color definitions

.. role:: black
.. role:: gray 
.. role:: silver 
.. role:: white 
.. role:: maroon 
.. role:: red 
.. role:: magenta
.. role:: fuchsia
.. role:: pink
.. role:: orange
.. role:: yellow
.. role:: lime
.. role:: green
.. role:: olive
.. role:: steel
.. role:: cyan
.. role:: aqua
.. role:: blue
.. role:: navy
.. role:: purple

.. text attributes

.. role:: underline

.. special characters

.. |(c)| unicode:: 0xA9 .. copyright sign
  :ltrim:
.. |(r)| unicode:: 0xAE   .. registered sign
  :ltrim:
.. |(tm)| unicode:: 0x2122   .. trademark sign
  :ltrim: